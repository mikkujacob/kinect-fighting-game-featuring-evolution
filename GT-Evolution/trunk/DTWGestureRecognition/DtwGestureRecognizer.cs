﻿//-----------------------------------------------------------------------
// <copyright file="DtwGestureRecognizer.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
// </copyright>
//-----------------------------------------------------------------------

using System.Diagnostics;

namespace DTWGestureRecognition
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    using WindowsInput.Native;

    /// <summary>
    /// Dynamic Time Warping nearest neighbour sequence comparison class.
    /// Called 'Gesture Recognizer' but really it can work with any vectors
    /// </summary>
    internal class DtwGestureRecognizer
    {
        /*
         * By Rhemyst. Dude's a freakin' genius. Also he can do the Rubik's Cube. I mean REALLY do the Rubik's Cube.
         * 
         * http://social.msdn.microsoft.com/Forums/en-US/kinectsdknuiapi/thread/4a428391-82df-445a-a867-557f284bd4b1
         * http://www.youtube.com/watch?v=XsIoN96yF3E
         */

        /// <summary>
        /// Size of obeservations vectors.
        /// </summary>
        private readonly int _dimension;

        /// <summary>
        /// Maximum distance between the last observations of each sequence.
        /// </summary>
        private readonly double _firstThreshold;

        /// <summary>
        /// Minimum length of a gesture before it can be recognised
        /// </summary>
        private readonly double _minimumLength;

        /// <summary>
        /// Maximum DTW distance between an example and a sequence being classified.
        /// </summary>
        private readonly double _globalThreshold;

        /// <summary>
        /// Minimum distance jumped to qualify as a jump
        /// </summary>
        private readonly double _jumpThreshold;
        
        /// <summary>
        /// Minimum distance crouched to qualify as a crouch
        /// </summary>
        private readonly double _crouchThreshold;

        /// <summary>
        /// The gesture names. Index matches that of the sequences array in _sequences
        /// </summary>
        private readonly ArrayList _labels;

        /// <summary>
        /// The gesture names. Index matches that of the sequences array in _sequences
        /// </summary>
        private readonly ArrayList _joints;

        private string VerticalState;

        /// <summary>
        /// Maximum vertical or horizontal steps in a row.
        /// </summary>
        private readonly int _maxSlope;

        /// <summary>
        /// The recorded gesture sequences
        /// </summary>
        private readonly ArrayList _sequences;

        /// <summary>
        /// Dictionary to enable conversion from Joint Name strings to index of coordinate data not to ignore
        /// </summary>
        private Dictionary<string, int> JointsCheckedToIndex = new Dictionary<string, int>
        {
            {"HandLeft", 0},
            {"WristLeft", 1},
            {"ElbowLeft", 2},
            {"HandRight", 3},
            {"WristRight", 4},
            {"ElbowRight", 5},
            {"ShoulderLeft", 6},
            {"ShoulderRight", 7},
            {"FootLeft", 8},
            {"AnkleLeft", 9},
            {"KneeLeft", 10},
            {"FootRight", 11},
            {"AnkleRight", 12},
            {"KneeRight", 13},
            {"HipLeft", 14},
            {"HipRight", 15},
            {"Head", 16},
            {"Spine", 17}
        };
        
        /// <summary>
        /// Initializes a new instance of the DtwGestureRecognizer class
        /// First DTW constructor
        /// </summary>
        /// <param name="dim">Vector size</param>
        /// <param name="threshold">Maximum distance between the last observations of each sequence</param>
        /// <param name="firstThreshold">Minimum threshold</param>
        public DtwGestureRecognizer(int dim, double threshold, double firstThreshold, double minLen)
        {
            _dimension = dim;
            _sequences = new ArrayList();
            _labels = new ArrayList();
            _joints = new ArrayList();
            _globalThreshold = threshold;
            _firstThreshold = firstThreshold;
            _maxSlope = int.MaxValue;
            _minimumLength = minLen;
            VerticalState = "Neutral";
            _jumpThreshold = 0.2;
            _crouchThreshold = 0.3;
        }

        /// <summary>
        /// Initializes a new instance of the DtwGestureRecognizer class
        /// Second DTW constructor
        /// </summary>
        /// <param name="dim">Vector size</param>
        /// <param name="threshold">Maximum distance between the last observations of each sequence</param>
        /// <param name="firstThreshold">Minimum threshold</param>
        /// <param name="ms">Maximum vertical or horizontal steps in a row</param>
        public DtwGestureRecognizer(int dim, double threshold, double firstThreshold, int ms, double minLen)
        {
            _dimension = dim;
            _sequences = new ArrayList();
            _labels = new ArrayList();
            _joints = new ArrayList();
            _globalThreshold = threshold;
            _firstThreshold = firstThreshold;
            _maxSlope = ms;
            _minimumLength = minLen;
            VerticalState = "Neutral";
            _jumpThreshold = 0.20;
            _crouchThreshold = 0.35;
        }

        /// <summary>
        /// Add a seqence with a label to the known sequences library.
        /// The gesture MUST start on the first observation of the sequence and end on the last one.
        /// Sequences may have different lengths.
        /// </summary>
        /// <param name="seq">The sequence</param>
        /// <param name="lab">Sequence name</param>
        public void AddOrUpdate(ArrayList seq, string lab, string[] joints)
        {
            // First we check whether there is already a recording for this label. If so overwrite it, otherwise add a new entry
            int existingIndex = -1;

            for (int i = 0; i < _labels.Count; i++)
            {
                if ((string)_labels[i] == lab)
                {
                    existingIndex = i;
                }
            }

            // If we have a match then remove the entries at the existing index to avoid duplicates. We will add the new entries later anyway
            if (existingIndex >= 0)
            {
                _sequences.RemoveAt(existingIndex);
                _labels.RemoveAt(existingIndex);
                _joints.RemoveAt(existingIndex);
            }

            int index = 0;

            //Order by number of joints checked
            for (; index < _sequences.Count; index++)
            {
                if (((string[])(_joints[index])).Length < joints.Length)
                {
                    break;
                }
            }
            // Insert the new entries at correct position
            _joints.Insert(index, joints);
            _sequences.Insert(index, MaskUncheckedJoints(seq, index));
            _labels.Insert(index, lab);
        }

        /// <summary>
        /// Recognize gesture in the given sequence.
        /// It will always assume that the gesture ends on the last observation of that sequence.
        /// If the distance between the last observations of each sequence is too great, or if the overall DTW distance between the two sequence is too great, no gesture will be recognized.
        /// </summary>
        /// <param name="seq">The sequence to recognise</param>
        /// <returns>The recognised gesture name</returns>
        public string Recognize(ArrayList seq)
        {
            double minDist = double.PositiveInfinity;
            string classification = "__UNKNOWN";
            for (int i = 0; i < _sequences.Count; i++)
            {
                string[] currentJointsToCheck = (string[])_joints[i];
                var example = new ArrayList((ArrayList) _sequences[i]);
                var seqReplaced = new ArrayList();

                using (MemoryStream stream = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, seq);
                    stream.Seek(0, SeekOrigin.Begin);
                    seqReplaced = (ArrayList)formatter.Deserialize(stream);
                    stream.Close();
                }

                seqReplaced = MaskUncheckedJoints(seqReplaced, i);

                ////Debug.WriteLine(Dist2((double[]) seq[seq.Count - 1], (double[]) example[example.Count - 1]));
                if (Dist2((double[]) seqReplaced[seqReplaced.Count - 1], (double[]) example[example.Count - 1]) < _firstThreshold)
                {
                    double d = Dtw(seqReplaced, example) / example.Count;
                    if (d < minDist)
                    {
                        minDist = d;
                        classification = (string)_labels[i];
                    }
                }
            }

            return (minDist < _globalThreshold ? classification : JumpCrouchCheckHook(seq)) + " " /*+minDist.ToString()*/;
        } 

        /// <summary>
        /// Mask joint values of unchecked joints to 0.0 to prevent them from interfering in Recognition of Gestures where they are unimportant
        /// </summary>
        /// <param name="seq">Sequence of Position Frames to mask</param>
        /// <param name="indexInJointList">Index into List of Joints that are important for that Gesture and hence NOT masked</param>
        /// <returns></returns>

        public ArrayList MaskUncheckedJoints(ArrayList seq, int indexInJointList)
        {
            Boolean[] isCompared = new Boolean[_dimension / 3];
            Boolean jointCheckingValid = false;
            string[] currentJointsToCheck = ((string[])_joints[indexInJointList]);

            if ( currentJointsToCheck[0] != "All")
            {
                jointCheckingValid = true;
                int index;
                foreach (string s in currentJointsToCheck)
                {
                    JointsCheckedToIndex.TryGetValue(s, out index);
                    isCompared[index] = true;
                }
            }

            if (jointCheckingValid)
            {
                foreach (double[] frame in seq)
                {
                    for (int index = 0; index < isCompared.Length; index++)
                    {
                        if (!isCompared[index])
                        {
                            frame[index * 3] = 0.0;
                            frame[index * 3 + 1] = 0.0;
                            frame[index * 3 + 2] = 0.0;
                        }
                    }
                }
            }

            return seq;
        }

        /// <summary>
        /// Retrieves a text represeantation of the _label and its associated _sequence
        /// For use in dispaying debug information and for saving to file
        /// </summary>
        /// <returns>A string containing all recorded gestures and their names</returns>
        public string RetrieveText()
        {
            string retStr = String.Empty;

            if (_sequences != null)
            {
                // Iterate through each gesture
                for (int gestureNum = 0; gestureNum < _sequences.Count; gestureNum++)
                {
                    // Echo the label
                    retStr += _labels[gestureNum] + "\r\n";

                    /*Uncomment when actual joint selection is implemented in
                     * Slight error in formatting text output
                     * 
                     */ 
                    retStr += "+";

                    foreach (string s in ((string[])_joints[gestureNum]))
                    {
                        retStr += s + ",";
                    }

                    retStr = retStr.Substring(0, retStr.Length - 1);

                    retStr += "\r\n";

                    int frameNum = 0;

                    //Iterate through each frame of this gesture
                    foreach (double[] frame in ((ArrayList)_sequences[gestureNum]))
                    {
                        // Extract each double
                        foreach (double dub in (double[])frame)
                        {
                            retStr += dub + "\r\n";
                        }

                        // Signifies end of this double
                        retStr += "~\r\n";

                        frameNum++;
                    }

                    // Signifies end of this gesture
                    retStr += "----";
                    if (gestureNum < _sequences.Count - 1)
                    {
                        retStr += "\r\n";
                    }
                }
            }

            return retStr;
        }

        public string JumpCrouchCheckHook(ArrayList seq)
        {
            string result = "__UNKNOWN";
            int SpineIndex;
            JointsCheckedToIndex.TryGetValue("Spine", out SpineIndex);
            double SpineYAtZero = ((double[])(seq[0]))[3 * SpineIndex + 1];

            foreach (double[] frame in seq)
            {
                if (frame[3 * SpineIndex + 1] - SpineYAtZero > _jumpThreshold)
                {
                    if (VerticalState.Equals("Neutral"))
                    {
                        result = "@Jump";
                        VerticalState = "Jump";
                        break;
                    }
                    else
                    {
                        result = "@Crouch Up";
                        VerticalState = "Neutral";
                        break;
                    }
                    
                }
            }

            if (result.Equals("__UNKNOWN"))
            {
                foreach (double[] frame in seq)
                {
                    if (SpineYAtZero - frame[3 * SpineIndex + 1] > _crouchThreshold && VerticalState.Equals("Neutral"))
                    {
                        result = "@Crouch Down";
                        VerticalState = "Crouch";
                        break;
                    }
                }
            }

            if (result.Equals("@Jump"))
            {
                VerticalState = "Neutral";
            }

            return (result);
        }

        /// <summary>
        /// Compute the min DTW distance between seq2 and all possible endings of seq1.
        /// </summary>
        /// <param name="seq1">The first array of sequences to compare</param>
        /// <param name="seq2">The second array of sequences to compare</param>
        /// <returns>The best match</returns>
        public double Dtw(ArrayList seq1, ArrayList seq2)
        {
            // Init
            var seq1R = new ArrayList(seq1);
            seq1R.Reverse();
            var seq2R = new ArrayList(seq2);
            seq2R.Reverse();
            var tab = new double[seq1R.Count + 1, seq2R.Count + 1];
            var slopeI = new int[seq1R.Count + 1, seq2R.Count + 1];
            var slopeJ = new int[seq1R.Count + 1, seq2R.Count + 1];

            for (int i = 0; i < seq1R.Count + 1; i++)
            {
                for (int j = 0; j < seq2R.Count + 1; j++)
                {
                    tab[i, j] = double.PositiveInfinity;
                    slopeI[i, j] = 0;
                    slopeJ[i, j] = 0;
                }
            }

            tab[0, 0] = 0;

            // Dynamic computation of the DTW matrix.
            for (int i = 1; i < seq1R.Count + 1; i++)
            {
                for (int j = 1; j < seq2R.Count + 1; j++)
                {
                    if (tab[i, j - 1] < tab[i - 1, j - 1] && tab[i, j - 1] < tab[i - 1, j] &&
                        slopeI[i, j - 1] < _maxSlope)
                    {
                        tab[i, j] = Dist2((double[]) seq1R[i - 1], (double[]) seq2R[j - 1]) + tab[i, j - 1];
                        slopeI[i, j] = slopeJ[i, j - 1] + 1;
                        slopeJ[i, j] = 0;
                    }
                    else if (tab[i - 1, j] < tab[i - 1, j - 1] && tab[i - 1, j] < tab[i, j - 1] &&
                             slopeJ[i - 1, j] < _maxSlope)
                    {
                        tab[i, j] = Dist2((double[]) seq1R[i - 1], (double[]) seq2R[j - 1]) + tab[i - 1, j];
                        slopeI[i, j] = 0;
                        slopeJ[i, j] = slopeJ[i - 1, j] + 1;
                    }
                    else
                    {
                        tab[i, j] = Dist2((double[]) seq1R[i - 1], (double[]) seq2R[j - 1]) + tab[i - 1, j - 1];
                        slopeI[i, j] = 0;
                        slopeJ[i, j] = 0;
                    }
                }
            }

            // Find best between seq2 and an ending (postfix) of seq1.
            double bestMatch = double.PositiveInfinity;
            for (int i = 1; i < (seq1R.Count + 1) - _minimumLength; i++)
            {
                if (tab[i, seq2R.Count] < bestMatch)
                {
                    bestMatch = tab[i, seq2R.Count];
                }
            }

            return bestMatch;
        }

        /// <summary>
        /// Computes a 1-distance between two observations. (aka Manhattan distance).
        /// </summary>
        /// <param name="a">Point3D a (double)</param>
        /// <param name="b">Point3D b (double)</param>
        /// <returns>Manhattan distance between the two points</returns>
        private double Dist1(double[] a, double[] b)
        {
            double d = 0;
            for (int i = 0; i < _dimension; i++)
            {
                d += Math.Abs(a[i] - b[i]);
            }

            return d;
        }

        /// <summary>
        /// Computes a 2-distance between two observations. (aka Euclidian distance).
        /// </summary>
        /// <param name="a">Point3D a (double)</param>
        /// <param name="b">Point3D b (double)</param>
        /// <returns>Euclidian distance between the two points</returns>
        private double Dist2(double[] a, double[] b)
        {
            double d = 0;
            for (int i = 0; i < _dimension; i++)
            {
                d += Math.Pow(a[i] - b[i], 2);
            }

            return Math.Sqrt(d);
        }
    }
}