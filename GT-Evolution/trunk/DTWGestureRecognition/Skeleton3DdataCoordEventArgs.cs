﻿//-----------------------------------------------------------------------
// <copyright file="Skeleton3DdataCoordEventArgs.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
// </copyright>
//-----------------------------------------------------------------------

namespace DTWGestureRecognition
{
    using System.Windows;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Takes Kinect SDK Skeletal Frame coordinates and converts them intoo a format useful to th DTW
    /// </summary>
    internal class Skeleton3DdataCoordEventArgs
    {
        /// <summary>
        /// Positions of the elbows, the wrists and the hands (placed from left to right)
        /// </summary>
        private readonly Point3D[,] _points;

        /// <summary>
        /// 
        /// </summary>
        private bool isEmptyP1;

        /// <summary>
        /// 
        /// </summary>
        private bool isEmptyP2;

        /// <summary>
        /// Initializes a new instance of the Skeleton3DdataCoordEventArgs class
        /// </summary>
        /// <param name="points">The points we need to handle in this class</param>
        public Skeleton3DdataCoordEventArgs(Point3D[,] points, bool isEmptyP1, bool isEmptyP2)
        {
            _points = (Point3D[,]) points.Clone();

            this.isEmptyP1 = isEmptyP1;
            this.isEmptyP2 = isEmptyP2;
        }


        public bool isEmpty(int pindex)
        {
            if (pindex == 0)
            {
                return isEmptyP1;
            }
            else if (pindex == 1)
            {
                return isEmptyP2;
            }

            return true;
        }

        /// <summary>
        /// Gets the point at a certain index
        /// </summary>
        /// <param name="index">The index we wish to retrieve</param>
        /// <returns>The point at the sent index</returns>
        public Point3D GetPoint(int index, int pindex)
        {
            return _points[pindex, index];
        }

        /// <summary>
        /// Gets the coordinates of our _points
        /// </summary>
        /// <returns>The coordinates of our _points</returns>
        internal double[] GetCoords(int pindex)
        {
            var tmp = new double[_points.GetLength(1) * 3];
            for (int i = 0; i < _points.GetLength(1); i++)
            {
                tmp[3 * i] = _points[pindex, i].X;
                tmp[(3 * i) + 1] = _points[pindex, i].Y;
                tmp[(3 * i) + 2] = _points[pindex, i].Z;
            }

            return tmp;
        }
    }
}