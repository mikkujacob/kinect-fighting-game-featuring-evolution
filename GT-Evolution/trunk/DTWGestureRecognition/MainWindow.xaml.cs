﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
// </copyright>
//-----------------------------------------------------------------------

using System;
using Microsoft.Research.Kinect.Nui;
using Kinect.Extensions;

namespace DTWGestureRecognition
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Microsoft.Research.Kinect.Nui;
    using System.Windows.Media.Media3D;
    using WindowsInput.Native;
    using WindowsInput;
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        // We want to control how depth data gets converted into false-color data
        // for more intuitive visualization, so we keep 32-bit color frame buffer versions of
        // these, to be updated whenever we receive and process a 16-bit frame.

        /// <summary>
        /// The red index
        /// </summary>
        private const int RedIdx = 2;

        /// <summary>
        /// The green index
        /// </summary>
        private const int GreenIdx = 1;

        /// <summary>
        /// The blue index
        /// </summary>
        private const int BlueIdx = 0;

        /// <summary>
        /// How many skeleton frames to ignore (_flipFlop)
        /// 1 = capture every frame, 2 = capture every second frame etc.
        /// </summary>
        private const int Ignore = 2;

        /// <summary>
        /// How many skeleton frames to store in the _video buffer
        /// </summary>
        private const int Buffer1Size = 32;

        /// <summary>
        /// How many skeleton frames to store in the _video buffer
        /// </summary>
        private const int Buffer2Size = 32;

        /// <summary>
        /// The minumum number of frames in the _video buffer before we attempt to start matching gestures
        /// </summary>
        private const int MinimumFrames = 6;

        /// <summary>
        /// The minumum number of frames in the _video buffer before we attempt to start matching gestures
        /// </summary>
        private const int CaptureCountdownSeconds = 3;

        /// <summary>
        /// Where we will save our gestures to. The app will append a data/time and .txt to this string
        /// </summary>
        private const string GestureSaveFileLocation = @"C:\DTWGestureRecognition\DTWRecordedGestures\";

        /// <summary>
        /// Where we will save our gestures to. The app will append a data/time and .txt to this string
        /// </summary>
        private const string GestureSaveFileNamePrefix = @"RecordedGestures";

        /// <summary>
        /// Where the XNA Fighter Engine is located
        /// </summary>
        private const string XNAFighterEngineFileLocationAndExe = @"C:\XNAFighterEngine\XNAFighterEngine\bin\x86\Debug Use From File Loading\XNAFighterEngine.exe";

        /// <summary>
        /// Maximum number of joints that could be monitored by the program
        /// </summary>
        private const int MaxNumberOfJointsMonitored = 18;

        /// <summary>
        /// Dictionary of all the joints Kinect SDK is capable of tracking. You might not want always to use them all but they are included here for thouroughness.
        /// </summary>
        private readonly Dictionary<JointID, Brush> _jointColors = new Dictionary<JointID, Brush>
        { 
            {JointID.HipCenter, new SolidColorBrush(Color.FromRgb(169, 176, 155))},
            {JointID.Spine, new SolidColorBrush(Color.FromRgb(169, 176, 155))},
            {JointID.ShoulderCenter, new SolidColorBrush(Color.FromRgb(168, 230, 29))},
            {JointID.Head, new SolidColorBrush(Color.FromRgb(200, 0, 0))},
            {JointID.ShoulderLeft, new SolidColorBrush(Color.FromRgb(79, 84, 33))},
            {JointID.ElbowLeft, new SolidColorBrush(Color.FromRgb(84, 33, 42))},
            {JointID.WristLeft, new SolidColorBrush(Color.FromRgb(255, 126, 0))},
            {JointID.HandLeft, new SolidColorBrush(Color.FromRgb(215, 86, 0))},
            {JointID.ShoulderRight, new SolidColorBrush(Color.FromRgb(33, 79,  84))},
            {JointID.ElbowRight, new SolidColorBrush(Color.FromRgb(33, 33, 84))},
            {JointID.WristRight, new SolidColorBrush(Color.FromRgb(77, 109, 243))},
            {JointID.HandRight, new SolidColorBrush(Color.FromRgb(37,  69, 243))},
            {JointID.HipLeft, new SolidColorBrush(Color.FromRgb(77, 109, 243))},
            {JointID.KneeLeft, new SolidColorBrush(Color.FromRgb(69, 33, 84))},
            {JointID.AnkleLeft, new SolidColorBrush(Color.FromRgb(229, 170, 122))},
            {JointID.FootLeft, new SolidColorBrush(Color.FromRgb(255, 126, 0))},
            {JointID.HipRight, new SolidColorBrush(Color.FromRgb(181, 165, 213))},
            {JointID.KneeRight, new SolidColorBrush(Color.FromRgb(71, 222, 76))},
            {JointID.AnkleRight, new SolidColorBrush(Color.FromRgb(245, 228, 156))},
            {JointID.FootRight, new SolidColorBrush(Color.FromRgb(77, 109, 243))}
        };

        /// <summary>
        /// The depth frame byte array. Only supports 320 * 240 at this time
        /// </summary>
        private readonly byte[] _depthFrame32 = new byte[320 * 240 * 4];

        /// <summary>
        /// Flag to show whether or not the gesture recogniser is capturing a new pose
        /// </summary>
        private bool _capturing;

        /// <summary>
        /// Dynamic Time Warping object
        /// </summary>
        private DtwGestureRecognizer _dtw;

        /// <summary>
        /// How many frames occurred 'last time'. Used for calculating frames per second
        /// </summary>
        private int _lastFrames;

        /// <summary>
        /// The 'last time' DateTime. Used for calculating frames per second
        /// </summary>
        private DateTime _lastTime = DateTime.MaxValue;

        /// <summary>
        /// The Natural User Interface runtime
        /// </summary>
        private Runtime _nui;

        /// <summary>
        /// 
        /// </summary>
        public static int angle;

        /// <summary>
        /// Total number of framed that have occurred. Used for calculating frames per second
        /// </summary>
        private int _totalFrames;

        /// <summary>
        /// Switch used to ignore certain skeleton frames
        /// </summary>
        private int _flipFlopP1;

        /// <summary>
        /// Switch used to ignore certain skeleton frames
        /// </summary>
        private int _flipFlopP2;

        /// <summary>
        /// ArrayList of coordinates which are recorded in sequence to define one gesture for Player 1
        /// </summary>
        private ArrayList _video1;

        /// <summary>
        /// ArrayList of coordinates which are recorded in sequence to define one gesture for Player 2
        /// </summary>
        private ArrayList _video2;

        /// <summary>
        /// ArrayList of coordinates which are recorded in sequence to define one gesture
        /// </summary>
        private DateTime _captureCountdown = DateTime.Now;

        /// <summary>
        /// ArrayList of coordinates which are recorded in sequence to define one gesture
        /// </summary>
        private Timer _captureCountdownTimer;

        /// <summary>
        /// Variable to store Key Emulator class
        /// </summary>
        private KeyboardSimulator _sim;

        Boolean isGameStarted;

        /// <summary>
        /// Dictionary to enable conversion from Action Labels To Keyboard Characters
        /// </summary>
        private Dictionary<string, VirtualKeyCode> GestureToKeyMapP1 = new Dictionary<string, VirtualKeyCode>
        {
            //Don't forget special rules for handling Crouch Up, Jump Left, Jump Right

            {"Up", VirtualKeyCode.VK_W},
            {"Down", VirtualKeyCode.VK_S},
            {"Left", VirtualKeyCode.VK_A},
            {"Right", VirtualKeyCode.VK_D},
            {"Move Left", VirtualKeyCode.VK_A},
            {"Move Right", VirtualKeyCode.VK_D},
            {"Jump", VirtualKeyCode.VK_W},
            {"High Punch", VirtualKeyCode.VK_Y},
            {"Medium Punch", VirtualKeyCode.VK_T},
            {"Low Punch", VirtualKeyCode.VK_R},
            {"High Kick", VirtualKeyCode.VK_H},
            {"Medium Kick", VirtualKeyCode.VK_G},
            {"Low Kick", VirtualKeyCode.VK_F},
            {"Block", VirtualKeyCode.VK_R},
            {"Select", VirtualKeyCode.RETURN},
            {"Back", VirtualKeyCode.ESCAPE}
        };

        /// <summary>
        /// Dictionary to enable conversion from Action Labels To Keyboard Characters
        /// </summary>
        private Dictionary<string, VirtualKeyCode> GestureToKeyMapP2 = new Dictionary<string, VirtualKeyCode>
        {
            //Don't forget special rules for handling Crouch Up, Jump Left, Jump Right

            {"Up", VirtualKeyCode.UP},
            {"Down", VirtualKeyCode.DOWN},
            {"Left", VirtualKeyCode.LEFT},
            {"Right", VirtualKeyCode.RIGHT},
            {"Move Left", VirtualKeyCode.LEFT},
            {"Move Right", VirtualKeyCode.RIGHT},
            {"Jump", VirtualKeyCode.UP},
            {"High Punch", VirtualKeyCode.VK_O},
            {"Medium Punch", VirtualKeyCode.VK_I},
            {"Low Punch", VirtualKeyCode.VK_U},
            {"High Kick", VirtualKeyCode.VK_L},
            {"Medium Kick", VirtualKeyCode.VK_K},
            {"Low Kick", VirtualKeyCode.VK_J},
            {"Block", VirtualKeyCode.VK_U},
            {"Select", VirtualKeyCode.SPACE},
            {"Back", VirtualKeyCode.DELETE}
        };

        /// <summary>
        /// Initializes a new instance of the MainWindow class
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Opens the sent text file and creates a _dtw recorded gesture sequence
        /// Currently not very flexible and totally intolerant of errors.
        /// </summary>
        /// <param name="fileLocation">Full path to the gesture file</param>
        public void LoadGesturesFromFile(string fileLocation)
        {
            int itemCount = 0;
            string line;
            string gestureName = String.Empty;
            string[] jointsChecked = {"All"};

            // TODO I'm defaulting this to 18 here for now as it meets my current need but I need to cater for variable lengths in the future
            ArrayList frames = new ArrayList();
            double[] items = new double[MaxNumberOfJointsMonitored * 3];

            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader(fileLocation);
            while ((line = file.ReadLine()) != null)
            {
                if (line.StartsWith("@"))
                {
                    gestureName = line;
                    continue;
                }

                if (line.StartsWith("+"))
                {
                    if ((string)(line.Substring(1)) != "All\r\n")
                    {
                        string tempJoints = line.Substring(1);
                        jointsChecked = tempJoints.Split(",".ToCharArray());
                    }

                    continue;
                }

                if (line.StartsWith("~"))
                {
                    frames.Add(items);
                    itemCount = 0;
                    items = new double[MaxNumberOfJointsMonitored * 3];
                    continue;
                }

                if (!line.StartsWith("----"))
                {
                    items[itemCount] = Double.Parse(line);
                }

                itemCount++;

                if (line.StartsWith("----"))
                {
                    _dtw.AddOrUpdate(frames, gestureName, jointsChecked);
                    frames = new ArrayList();
                    gestureName = String.Empty;
                    itemCount = 0;
                }
            }

            file.Close();
        }

        /// <summary>
        /// Called each time a skeleton frame is ready. Passes skeletal data to the DTW processor
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Skeleton Frame Ready Event Args</param>
        private static void SkeletonExtractSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            SkeletonFrame skeletonFrame = e.SkeletonFrame;

            ArrayList PlayerData = new ArrayList();

            foreach (SkeletonData data in skeletonFrame.Skeletons)
            {
                if (data.TrackingState == SkeletonTrackingState.Tracked)
                {
                    PlayerData.Add(data);
                }
            }

            if (PlayerData.Count == 1)
            {
                Skeleton3DDataExtract.ProcessData(MaxNumberOfJointsMonitored, (SkeletonData)PlayerData[0], null);
            }
            else if (PlayerData.Count == 2)
            {
                Skeleton3DDataExtract.ProcessData(MaxNumberOfJointsMonitored, (SkeletonData)PlayerData[0], (SkeletonData)PlayerData[1]);
            }

            PlayerData.Clear();
        }

        /// <summary>
        /// Converts a 16-bit grayscale depth frame which includes player indexes into a 32-bit frame that displays different players in different colors
        /// </summary>
        /// <param name="depthFrame16">The depth frame byte array</param>
        /// <returns>A depth frame byte array containing a player image</returns>
        private byte[] ConvertDepthFrame(byte[] depthFrame16)
        {
            for (int i16 = 0, i32 = 0; i16 < depthFrame16.Length && i32 < _depthFrame32.Length; i16 += 2, i32 += 4)
            {
                int player = depthFrame16[i16] & 0x07;
                int realDepth = (depthFrame16[i16 + 1] << 5) | (depthFrame16[i16] >> 3);
                
                // transform 13-bit depth information into an 8-bit intensity appropriate
                // for display (we disregard information in most significant bit)
                var intensity = (byte)(255 - (255 * realDepth / 0x0fff));

                _depthFrame32[i32 + RedIdx] = 0;
                _depthFrame32[i32 + GreenIdx] = 0;
                _depthFrame32[i32 + BlueIdx] = 0;

                // choose different display colors based on player
                switch (player)
                {
                    case 0:
                        _depthFrame32[i32 + RedIdx] = (byte)(intensity / 2);
                        _depthFrame32[i32 + GreenIdx] = (byte)(intensity / 2);
                        _depthFrame32[i32 + BlueIdx] = (byte)(intensity / 2);
                        break;
                    case 1:
                        _depthFrame32[i32 + RedIdx] = intensity;
                        break;
                    case 2:
                        _depthFrame32[i32 + GreenIdx] = intensity;
                        break;
                    case 3:
                        _depthFrame32[i32 + RedIdx] = (byte)(intensity / 4);
                        _depthFrame32[i32 + GreenIdx] = intensity;
                        _depthFrame32[i32 + BlueIdx] = intensity;
                        break;
                    case 4:
                        _depthFrame32[i32 + RedIdx] = intensity;
                        _depthFrame32[i32 + GreenIdx] = intensity;
                        _depthFrame32[i32 + BlueIdx] = (byte)(intensity / 4);
                        break;
                    case 5:
                        _depthFrame32[i32 + RedIdx] = intensity;
                        _depthFrame32[i32 + GreenIdx] = (byte)(intensity / 4);
                        _depthFrame32[i32 + BlueIdx] = intensity;
                        break;
                    case 6:
                        _depthFrame32[i32 + RedIdx] = (byte)(intensity / 2);
                        _depthFrame32[i32 + GreenIdx] = (byte)(intensity / 2);
                        _depthFrame32[i32 + BlueIdx] = intensity;
                        break;
                    case 7:
                        _depthFrame32[i32 + RedIdx] = (byte)(255 - intensity);
                        _depthFrame32[i32 + GreenIdx] = (byte)(255 - intensity);
                        _depthFrame32[i32 + BlueIdx] = (byte)(255 - intensity);
                        break;
                }
            }

            return _depthFrame32;
        }

        /// <summary>
        /// Called when each depth frame is ready
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Image Frame Ready Event Args</param>
        private void NuiDepthFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage image = e.ImageFrame.Image;
            byte[] convertedDepthFrame = ConvertDepthFrame(image.Bits);

            depthImage.Source = BitmapSource.Create(
                image.Width, image.Height, 96, 96, PixelFormats.Bgr32, null, convertedDepthFrame, image.Width * 4);

            ++_totalFrames;

            DateTime cur = DateTime.Now;
            if (cur.Subtract(_lastTime) > TimeSpan.FromSeconds(1))
            {
                int frameDiff = _totalFrames - _lastFrames;
                _lastFrames = _totalFrames;
                _lastTime = cur;
                frameRate.Text = frameDiff + " fps";
            }
        }

        /// <summary>
        /// Gets the display position (i.e. where in the display image) of a Joint
        /// </summary>
        /// <param name="joint">Kinect NUI Joint</param>
        /// <returns>Point mapped location of sent joint</returns>
        private Point GetDisplayPosition(Joint joint)
        {
            float depthX, depthY;
            _nui.SkeletonEngine.SkeletonToDepthImage(joint.Position, out depthX, out depthY);
            depthX = Math.Max(0, Math.Min(depthX * 320, 320)); // convert to 320, 240 space
            depthY = Math.Max(0, Math.Min(depthY * 240, 240)); // convert to 320, 240 space
            int colorX, colorY;
            var iv = new ImageViewArea();

            // Only ImageResolution.Resolution640x480 is supported at this point
            _nui.NuiCamera.GetColorPixelCoordinatesFromDepthPixel(ImageResolution.Resolution640x480, iv, (int)depthX, (int)depthY, 0, out colorX, out colorY);

            // Map back to skeleton.Width & skeleton.Height
            return new Point((int)(skeletonCanvas.Width * colorX / 640.0), (int)(skeletonCanvas.Height * colorY / 480));
        }

        /// <summary>
        /// Works out how to draw a line ('bone') for sent Joints
        /// </summary>
        /// <param name="joints">Kinect NUI Joints</param>
        /// <param name="brush">The brush we'll use to colour the joints</param>
        /// <param name="ids">The JointsIDs we're interested in</param>
        /// <returns>A line or lines</returns>
        private Polyline GetBodySegment(JointsCollection joints, Brush brush, params JointID[] ids)
        {
            var points = new PointCollection(ids.Length);
            foreach (JointID t in ids)
            {
                points.Add(GetDisplayPosition(joints[t]));
            }

            var polyline = new Polyline();
            polyline.Points = points;
            polyline.Stroke = brush;
            polyline.StrokeThickness = 5;
            return polyline;
        }

        /// <summary>
        /// Runs every time a skeleton frame is ready. Updates the skeleton canvas with new joint and polyline locations.
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Skeleton Frame Event Args</param>
        private void NuiSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            SkeletonFrame skeletonFrame = e.SkeletonFrame;
            int iSkeleton = 0;
            var brushes = new Brush[6];
            brushes[0] = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            brushes[1] = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            brushes[2] = new SolidColorBrush(Color.FromRgb(64, 255, 255));
            brushes[3] = new SolidColorBrush(Color.FromRgb(255, 255, 64));
            brushes[4] = new SolidColorBrush(Color.FromRgb(255, 64, 255));
            brushes[5] = new SolidColorBrush(Color.FromRgb(128, 128, 255));

            skeletonCanvas.Children.Clear();
            foreach (SkeletonData data in skeletonFrame.Skeletons)
            {
     

                if (SkeletonTrackingState.Tracked == data.TrackingState)
                {
                    //Console.WriteLine("Tracking State" + data.TrackingState);
                    //Console.WriteLine("Tracking ID" + data.TrackingID);
                    //Console.WriteLine("User Index" + data.UserIndex);
                    //Console.WriteLine("--------------------------");
                    // Draw bones
                    Brush brush = brushes[iSkeleton % brushes.Length];
                    skeletonCanvas.Children.Add(GetBodySegment(data.Joints, brush, JointID.HipCenter, JointID.Spine, JointID.ShoulderCenter, JointID.Head));
                    skeletonCanvas.Children.Add(GetBodySegment(data.Joints, brush, JointID.ShoulderCenter, JointID.ShoulderLeft, JointID.ElbowLeft, JointID.WristLeft, JointID.HandLeft));
                    skeletonCanvas.Children.Add(GetBodySegment(data.Joints, brush, JointID.ShoulderCenter, JointID.ShoulderRight, JointID.ElbowRight, JointID.WristRight, JointID.HandRight));
                    skeletonCanvas.Children.Add(GetBodySegment(data.Joints, brush, JointID.HipCenter, JointID.HipLeft, JointID.KneeLeft, JointID.AnkleLeft, JointID.FootLeft));
                    skeletonCanvas.Children.Add(GetBodySegment(data.Joints, brush, JointID.HipCenter, JointID.HipRight, JointID.KneeRight, JointID.AnkleRight, JointID.FootRight));
                    
                    // Draw joints
                    foreach (Joint joint in data.Joints)
                    {
                        Point jointPos = GetDisplayPosition(joint);
                        var jointLine = new Line();
                        jointLine.X1 = jointPos.X - 3;
                        jointLine.X2 = jointLine.X1 + 6;
                        jointLine.Y1 = jointLine.Y2 = jointPos.Y;
                        jointLine.Stroke = _jointColors[joint.ID];
                        jointLine.StrokeThickness = 6;
                        skeletonCanvas.Children.Add(jointLine);
                    }
                }

                iSkeleton++;
            } // for each skeleton
        }

        /// <summary>
        /// Called every time a video (RGB) frame is ready
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Image Frame Ready Event Args</param>
        private void NuiColorFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            // 32-bit per pixel, RGBA image
            PlanarImage image = e.ImageFrame.Image;
            videoImage.Source = BitmapSource.Create(
                image.Width, image.Height, 96, 96, PixelFormats.Bgr32, null, image.Bits, image.Width * image.BytesPerPixel);
        }

        /// <summary>
        /// Runs after the window is loaded
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            _nui = new Runtime();

            _sim = new KeyboardSimulator();

            try
            {
                _nui.Initialize(RuntimeOptions.UseDepthAndPlayerIndex | RuntimeOptions.UseSkeletalTracking |
                               RuntimeOptions.UseColor);
            }
            catch (InvalidOperationException)
            {
                System.Windows.MessageBox.Show("Runtime initialization failed. Please make sure Kinect device is plugged in.");
                return;
            }

            try
            {
                _nui.VideoStream.Open(ImageStreamType.Video, 2, ImageResolution.Resolution640x480, ImageType.Color);
                _nui.DepthStream.Open(ImageStreamType.Depth, 2, ImageResolution.Resolution320x240, ImageType.DepthAndPlayerIndex);
            }
            catch (InvalidOperationException)
            {
                System.Windows.MessageBox.Show(
                    "Failed to open stream. Please make sure to specify a supported image type and resolution.");
                return;
            }

            _lastTime = DateTime.Now;

            _dtw = new DtwGestureRecognizer(MaxNumberOfJointsMonitored * 3, 0.4, 2, 2, 10);
            _video1 = new ArrayList();
            _video2 = new ArrayList();

            // If you want to see the depth image and frames per second then include this
            // I'mma turn this off 'cos my 'puter is proper slow
            _nui.DepthFrameReady += NuiDepthFrameReady;

            _nui.SkeletonFrameReady += NuiSkeletonFrameReady;
            _nui.SkeletonFrameReady += SkeletonExtractSkeletonFrameReady;

            // If you want to see the RGB stream then include this
            _nui.VideoFrameReady += NuiColorFrameReady;

            Skeleton3DDataExtract.Skeleton3DdataCoordReady += NuiSkeleton3DdataCoordReady;

            // Update the debug window with Sequences information
            dtwTextOutput.Text = _dtw.RetrieveText();

            angle = -5;
            if (_nui.NuiCamera.TrySetAngle(angle))
                Debug.WriteLine("Moving the sensor..");
            else
                Debug.WriteLine("Error occured moving the sensor..");

            _nui.SkeletonEngine.TransformSmooth = false;

            Debug.WriteLine("Finished Window Loading");
        }

        /// <summary>
        /// Runs some tidy-up code when the window is closed. This is especially important for our NUI instance because the Kinect SDK is very picky about this having been disposed of nicely.
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Event Args</param>
        private void WindowClosed(object sender, EventArgs e)
        {
            Debug.WriteLine("Stopping NUI");
            _nui.Uninitialize();
            Debug.WriteLine("NUI stopped");
            Environment.Exit(0);
        }

        /// <summary>
        /// Converts recognized Gestures into Key Presses In another application
        /// </summary>
        /// <param name="s">Recognized Gesture Label</param>
        private void ExecuteKeyPress(string s, int PlayerNo)
        {
            s = s.Substring(1, s.Length - 2);
            if(s.Equals("Crouch Down"))
            {
                _sim.KeyDown(PlayerNo == 0 ? VirtualKeyCode.VK_S : VirtualKeyCode.DOWN);
                System.Console.WriteLine("Sent DwnS/DOWN by Player");
                _sim.KeyPress(VirtualKeyCode.VK_Z);   
            }
            else if(s.Equals("Crouch Up"))
            {
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_S : VirtualKeyCode.DOWN);
                System.Console.WriteLine("Sent PrsS/DOWN by Player");
                _sim.KeyPress(VirtualKeyCode.VK_Z);   
            }
            else if (s.Equals("Move Left Start"))
            {
                if (!(PlayerNo == 0 ? FlipLeftP1 : FlipLeftP2))
                {
                    _sim.KeyDown(PlayerNo == 0 ? VirtualKeyCode.VK_A : VirtualKeyCode.LEFT);
                    System.Console.WriteLine("Sent DwnA/LEFT by Player");
                }
                else
                {
                    _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_A : VirtualKeyCode.LEFT);
                    System.Console.WriteLine("Sent PrsA/LEFT by Player");
                }

                FlipLeftP1 = (PlayerNo == 0 ? !FlipLeftP1 : FlipLeftP1);
                FlipLeftP2 = (PlayerNo == 1 ? !FlipLeftP2 : FlipLeftP2);
                    
                _sim.KeyPress(VirtualKeyCode.VK_Z);
            }
            else if (s.Equals("Move Left End"))
            {
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_A : VirtualKeyCode.LEFT);
                System.Console.WriteLine("Sent PrsA/LEFT by Player 2");
                _sim.KeyPress(VirtualKeyCode.VK_Z);

                FlipLeftP1 = (PlayerNo == 0 ? false : FlipLeftP1);
                FlipLeftP2 = (PlayerNo == 1 ? false : FlipLeftP2);
            }
            else if (s.Equals("Move Right Start"))
            {
                if (!(PlayerNo == 0 ? FlipRightP1 : FlipRightP2))
                {
                    _sim.KeyDown(PlayerNo == 0 ? VirtualKeyCode.VK_D : VirtualKeyCode.RIGHT);
                    System.Console.WriteLine("Sent DwnD/RIGHT by Player 2");
                }
                else
                {
                    _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_D : VirtualKeyCode.RIGHT);
                    System.Console.WriteLine("Sent PrsD/RIGHT by Player 2");
                }

                FlipRightP1 = (PlayerNo == 0 ? !FlipRightP1 : FlipRightP1);
                FlipRightP2 = (PlayerNo == 1 ? !FlipRightP2 : FlipRightP2);

                _sim.KeyPress(VirtualKeyCode.VK_Z);
            }
            else if (s.Equals("Move Right End"))
            {
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_D : VirtualKeyCode.RIGHT);
                System.Console.WriteLine("Sent PrsS/DOWN by Player 2");
                _sim.KeyPress(VirtualKeyCode.VK_Z);

                FlipRightP1 = (PlayerNo == 0 ? false : FlipRightP1);
                FlipRightP2 = (PlayerNo == 1 ? false : FlipRightP2);
            }
            else if (s.Equals("Jump Left"))
            {
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_W : VirtualKeyCode.UP);
                System.Console.WriteLine("Sent PrsW/UP by Player " + PlayerNo);
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_A : VirtualKeyCode.LEFT);
                System.Console.WriteLine("Sent PrsA/LEFT by Player " + PlayerNo);
            }
            else if (s.Equals("Jump Right"))
            {
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_W : VirtualKeyCode.UP);
                System.Console.WriteLine("Sent PrsW/UP by Player " + PlayerNo);
                _sim.KeyPress(PlayerNo == 0 ? VirtualKeyCode.VK_D : VirtualKeyCode.RIGHT);
                System.Console.WriteLine("Sent PrsD/RIGHT by Player " + PlayerNo);
            }
            else if(GestureToKeyMapP1.ContainsKey(s))
            {
                VirtualKeyCode tempVK = new VirtualKeyCode();

                if(PlayerNo == 0) 
                {
                    GestureToKeyMapP1.TryGetValue(s, out tempVK);
                } 
                else
                {
                    GestureToKeyMapP2.TryGetValue(s, out tempVK);
                }

                _sim.KeyPress(tempVK);
                _sim.KeyPress(VirtualKeyCode.VK_Z);

                System.Console.WriteLine("Sent Prs" + tempVK);
            }

            System.Console.WriteLine("Recognized Gesture " + s + " by Player " + PlayerNo);
        }

        /// <summary>
        /// Runs every time our 3D coordinates are ready.
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="a">Skeleton 3Ddata Coord Event Args</param>
        private void NuiSkeleton3DdataCoordReady(object sender, Skeleton3DdataCoordEventArgs a)
        {
            if (!a.isEmpty(0))
            {
                currentBufferFrame.Text = "P1: " + _video1.Count.ToString();
            }
            if (!a.isEmpty(1))
            {
                currentBufferFramep2.Text = "P2: " + _video2.Count.ToString();
            }

            // We need a sensible number of frames before we start attempting to match gestures against remembered sequences
            if (!a.isEmpty(0) && _video1.Count > MinimumFrames && _capturing == false)
            {
                ////Debug.WriteLine("Reading and video.Count=" + video.Count);
                string s = _dtw.Recognize(_video1);
                results.Text = "P1: Recognised as: " + s;
                if (!s.Contains("__UNKNOWN"))
                {
                    // There was no match so reset the buffer
                    _video1 = new ArrayList();
                    if (isGameStarted)
                    {
                        ExecuteKeyPress(s, 0);
                    }
                }
            }

            // We need a sensible number of frames before we start attempting to match gestures against remembered sequences
            if (!a.isEmpty(1) && _video2.Count > MinimumFrames && _capturing == false)
            {
                ////Debug.WriteLine("Reading and video.Count=" + video.Count);
                string s = _dtw.Recognize(_video2);
                resultsp2.Text = "P2: Recognised as: " + s;
                if (!s.Contains("__UNKNOWN"))
                {
                    // There was a match so reset the buffer
                    _video2 = new ArrayList();
                    if (isGameStarted)
                    {
                        ExecuteKeyPress(s, 1);
                    }
                }
            }

            // Ensures that we remember only the last x frames
            if (!a.isEmpty(0) && _video1.Count > Buffer1Size)
            {
                // If we are currently capturing and we reach the maximum buffer size then automatically store
                if (_capturing)
                {
                    DtwStoreClick(null, null);
                }
                else
                {
                    // Remove the first frame in the buffer
                    _video1.RemoveAt(0);
                }
            }

            // Ensures that we remember only the last x frames
            if (!a.isEmpty(1) && _video2.Count > Buffer2Size)
            {
                // If we are currently capturing and we reach the maximum buffer size then automatically store
                if (_capturing)
                {
                    DtwStoreClick(null, null);
                }
                else
                {
                    // Remove the first frame in the buffer
                    _video2.RemoveAt(0);
                }
            }

            // Decide which skeleton frames to capture. Only do so if the frames actually returned a number. 
            // For some reason my Kinect/PC setup didn't always return a double in range (i.e. infinity) even when standing completely within the frame.
            // TODO Weird. Need to investigate this
            if (!a.isEmpty(0) && !double.IsNaN(a.GetPoint(0, 0).X))
            {
                // Optionally register only 1 frame out of every n
                _flipFlopP1 = (_flipFlopP1 + 1) % Ignore;
                if (_flipFlopP1 == 0)
                {
                    _video1.Add(a.GetCoords(0));
                }
            }
            else if (!a.isEmpty(0))
            {
                Console.Out.WriteLine("\nNaN received");
            }

            if (!a.isEmpty(1) && !double.IsNaN(a.GetPoint(0, 1).X))
            {
                // Optionally register only 1 frame out of every n
                _flipFlopP2 = (_flipFlopP2 + 1) % Ignore;
                if (_flipFlopP2 == 0)
                {
                    _video2.Add(a.GetCoords(1));
                }
            }

            else if (!a.isEmpty(1))
            {
                Console.Out.WriteLine("\nNaN received");
            }
        }

        /// <summary>
        /// Read mode. Sets our control variables and button enabled states
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void DtwReadClick(object sender, RoutedEventArgs e)
        {
            // Set the buttons enabled state
            dtwRead.IsEnabled = false;
            dtwCapture.IsEnabled = true;
            dtwStore.IsEnabled = false;

            // Set the capturing? flag
            _capturing = false;

            // Update the status display
            status.Text = "Reading";
        }

        /// <summary>
        /// Starts a countdown timer to enable the player to get in position to record gestures
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void DtwCaptureClick(object sender, RoutedEventArgs e)
        {
            _captureCountdown = DateTime.Now.AddSeconds(CaptureCountdownSeconds);

            _captureCountdownTimer = new Timer();
            _captureCountdownTimer.Interval = 50;
            _captureCountdownTimer.Start();
            _captureCountdownTimer.Tick += CaptureCountdown;
        }

        /// <summary>
        /// The method fired by the countdown timer. Either updates the countdown or fires the StartCapture method if the timer expires
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Event Args</param>
        private void CaptureCountdown(object sender, EventArgs e)
        {
            if (sender == _captureCountdownTimer)
            {
                if (DateTime.Now < _captureCountdown)
                {
                    status.Text = "Wait " + ((_captureCountdown - DateTime.Now).Seconds + 1) + " seconds";
                }
                else
                {
                    _captureCountdownTimer.Stop();
                    status.Text = "Recording gesture";
                    StartCapture();
                }
            }
        } 

        /// <summary>
        /// Capture mode. Sets our control variables and button enabled states
        /// </summary>
        private void StartCapture()
        {
            // Set the buttons enabled state
            dtwRead.IsEnabled = false;
            dtwCapture.IsEnabled = false;
            dtwStore.IsEnabled = true;

            // Set the capturing? flag
            _capturing = true;

            ////_captureCountdownTimer.Dispose();

            status.Text = "Recording gesture" + gestureList.Text;

            // Clear the _video buffer and start from the beginning
            _video1 = new ArrayList();
            _video2 = new ArrayList();
        }

        /// <summary>
        /// Stores our gesture to the DTW sequences list
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void DtwStoreClick(object sender, RoutedEventArgs e)
        {
            // Set the buttons enabled state
            dtwRead.IsEnabled = false;
            dtwCapture.IsEnabled = true;
            dtwStore.IsEnabled = false;

            // Set the capturing? flag
            _capturing = false;

            status.Text = "Remembering " + gestureList.Text;

            string[] joints = JointsCheckedForAction();
            // Add the current video buffer to the dtw sequences list
            _dtw.AddOrUpdate(_video1, gestureList.Text, joints);
            results.Text = "Gesture " + gestureList.Text + "added";

            // Update the debug window with Sequences information
            Debug.WriteLine(_dtw.RetrieveText());

            // Scratch the _video buffer
            _video1 = new ArrayList();

            // Switch back to Read mode
            DtwReadClick(null, null);
        }

        /// <summary>
        /// Stores our gesture to the DTW sequences list
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void DtwSaveToFile(object sender, RoutedEventArgs e)
        {
            string fileName = GestureSaveFileNamePrefix + DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + ".txt";
            System.IO.File.WriteAllText(GestureSaveFileLocation + fileName, _dtw.RetrieveText());
            status.Text = "Saved to " + fileName;
        }

        /// <summary>
        /// Loads the user's selected gesture file
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void DtwLoadFile(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";

            dlg.InitialDirectory = GestureSaveFileLocation;

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                LoadGesturesFromFile(dlg.FileName);
                dtwTextOutput.Text = _dtw.RetrieveText();
                status.Text = "Gestures loaded!";
            } 
        }

        /// <summary>
        /// Stores our gesture to the DTW sequences list
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Routed Event Args</param>
        private void DtwShowGestureText(object sender, RoutedEventArgs e)
        {
            dtwTextOutput.Text = _dtw.RetrieveText();
        }

        private string[] JointsCheckedForAction()
        {
            string[] result;
            ArrayList resultArray = new ArrayList();

            if (AllLeft.IsChecked == true && AllRight.IsChecked == true && Head.IsChecked == true && Spine.IsChecked == true)
            {
                resultArray.Add("All");
            }
            else if (AllLeft.IsChecked == true)
            {
                resultArray.Add("HandLeft");
                resultArray.Add("WristLeft");
                resultArray.Add("ElbowLeft");
                resultArray.Add("ShoulderLeft");
                resultArray.Add("FootLeft");
                resultArray.Add("AnkleLeft");
                resultArray.Add("KneeLeft");
                resultArray.Add("HipLeft");
                if (Head.IsChecked == true)
                {
                    resultArray.Add("Head");
                }
                if (Spine.IsChecked == true)
                {
                    resultArray.Add("Spine");
                }
            }
            else if (AllRight.IsChecked == true)
            {
                resultArray.Add("HandRight");
                resultArray.Add("WristRight");
                resultArray.Add("ElbowRight");
                resultArray.Add("ShoulderRight");
                resultArray.Add("FootRight");
                resultArray.Add("AnkleRight");
                resultArray.Add("KneeRight");
                resultArray.Add("HipRight");
                if (Head.IsChecked == true)
                {
                    resultArray.Add("Head");
                }
                if (Spine.IsChecked == true)
                {
                    resultArray.Add("Spine");
                }
            }
            else 
            {
                if (HandLeft.IsChecked == true)
                {
                    resultArray.Add("HandLeft");
                }
                if (WristLeft.IsChecked == true)
                {
                    resultArray.Add("WristLeft");
                }
                if (ElbowLeft.IsChecked == true)
                {
                    resultArray.Add("ElbowLeft");
                }
                if (ShoulderLeft.IsChecked == true)
                {
                    resultArray.Add("ShoulderLeft");
                }
                if (FootLeft.IsChecked == true)
                {
                    resultArray.Add("FootLeft");
                }
                if (AnkleLeft.IsChecked == true)
                {
                    resultArray.Add("AnkleLeft");
                }
                if (KneeLeft.IsChecked == true)
                {
                    resultArray.Add("KneeLeft");
                }
                if (HipLeft.IsChecked == true)
                {
                    resultArray.Add("HipLeft");
                }
                if (HandRight.IsChecked == true)
                {
                    resultArray.Add("HandRight");
                }
                if (WristRight.IsChecked == true)
                {
                    resultArray.Add("WristRight");
                }
                if (ElbowRight.IsChecked == true)
                {
                    resultArray.Add("ElbowRight");
                }
                if (ShoulderRight.IsChecked == true)
                {
                    resultArray.Add("ElbowRight");
                }
                if (FootRight.IsChecked == true)
                {
                    resultArray.Add("FootRight");
                }
                if (AnkleRight.IsChecked == true)
                {
                    resultArray.Add("AnkleRight");
                }
                if (KneeRight.IsChecked == true)
                {
                    resultArray.Add("KneeRight");
                }
                if (HipRight.IsChecked == true)
                {
                    resultArray.Add("KneeRight");
                }
                if (Head.IsChecked == true)
                {
                    resultArray.Add("Head");
                }
                if (Spine.IsChecked == true)
                {
                    resultArray.Add("Spine");
                }
            }

            if (resultArray.Count == 0)
            {
                resultArray.Add("All");
                AllLeft.IsChecked = true;
                AllRight.IsChecked = true;
            }

            result = new string[resultArray.Count];
            resultArray.CopyTo(result);

            return result;
        }

        private void AllLeft_Checked(object sender, RoutedEventArgs e)
        {
            HandLeft.IsChecked = false;
            WristLeft.IsChecked = false;
            ElbowLeft.IsChecked = false;
            ShoulderLeft.IsChecked = false;
            FootLeft.IsChecked = false;
            AnkleLeft.IsChecked = false;
            KneeLeft.IsChecked = false;
            HipLeft.IsChecked = false;
        }

        private void AllRight_Checked(object sender, RoutedEventArgs e)
        {
            HandRight.IsChecked = false;
            WristRight.IsChecked = false;
            ElbowRight.IsChecked = false;
            ShoulderLeft.IsChecked = false;
            FootRight.IsChecked = false;
            AnkleRight.IsChecked = false;
            KneeRight.IsChecked = false;
            HipLeft.IsChecked = false;
        }

        private void HandLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void WristLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void ElbowLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void FootLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void AnkleLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void KneeLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void HandRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void WristRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void ElbowRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void FootRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void AnkleRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void KneeRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void HipLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void ShoulderLeft_Checked(object sender, RoutedEventArgs e)
        {
            AllLeft.IsChecked = false;
        }

        private void HipRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void ShoulderRight_Checked(object sender, RoutedEventArgs e)
        {
            AllRight.IsChecked = false;
        }

        private void Spine_Checked(object sender, RoutedEventArgs e)
        {
            //Why Blank?
        }

        private void Head_Checked(object sender, RoutedEventArgs e)
        {
            //Why Blank?
        }

        private void StartGame_Click(object sender, RoutedEventArgs e)
        {
            if (isGameStarted)
            {
                return;
            }

            _sim.ModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.VK_R);
            System.Console.WriteLine("Sent DwnLWIN+PrsTAB+PrsTAB+UpLWIN");
            System.Threading.Thread.Sleep(1000);
            _sim.TextEntry(XNAFighterEngineFileLocationAndExe);
            System.Console.WriteLine("Sent TxtXNAFighterEngineLocation");
            System.Threading.Thread.Sleep(1000);
            _sim.KeyDown(VirtualKeyCode.RETURN);
            System.Console.WriteLine("Sent PrsReturn");
            //System.Threading.Thread.Sleep(120000);
            //_sim.KeyDown(VirtualKeyCode.RETURN);
            //System.Console.WriteLine("Sent PrsReturn");
            //System.Threading.Thread.Sleep(30000);
            //_sim.KeyDown(VirtualKeyCode.VK_S);
            //System.Console.WriteLine("Sent PrsS");
            //System.Threading.Thread.Sleep(5000);
            //_sim.KeyDown(VirtualKeyCode.VK_W);
            //System.Console.WriteLine("Sent PrsW");
            //System.Threading.Thread.Sleep(5000);
            //_sim.KeyDown(VirtualKeyCode.VK_S);
            //System.Console.WriteLine("Sent PrsS");
            //System.Threading.Thread.Sleep(5000);
            //_sim.KeyDown(VirtualKeyCode.VK_S);
            //System.Console.WriteLine("Sent PrsS");
            //System.Threading.Thread.Sleep(5000);
            //_sim.KeyDown(VirtualKeyCode.VK_S);
            //System.Console.WriteLine("Sent PrsS");

            isGameStarted = true;
        }

        private void CameraUp_Click(object sender, RoutedEventArgs e)
        {
            angle += 5;
            if (_nui.NuiCamera.TrySetAngle(angle))
                Debug.WriteLine("Moving the sensor..");
            else
                Debug.WriteLine("Error occured moving the sensor..");
        }

        private void CameraDown_Click(object sender, RoutedEventArgs e)
        {
            angle -= 5;
            if (_nui.NuiCamera.TrySetAngle(angle))
                Debug.WriteLine("Moving the sensor..");
            else
                Debug.WriteLine("Error occured moving the sensor..");
        }

        private void WTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Up ", 0);
        }

        private void ATest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Left ", 0);
        }

        private void STest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Down ", 0);
        }

        private void DTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Right ", 0);
        }

        private void RTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Low Punch ", 0);
        }

        private void TTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Medium Punch ", 0);
        }

        private void YTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@High Punch ", 0);
        }

        private void FTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Low Kick ", 0);
        }

        private void GTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Medium Kick ", 0);
        }

        private void HTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@High Kick ", 0);
        }

        private void RETURNTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Select ", 0);
        }

        private void ESCAPETest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Back ", 0);
        }

        private void UPTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Jump ", 1);
        }

        bool FlipLeftP1 = false;
        bool FlipLeftP2 = false;
        private void LEFTTest_Click(object sender, RoutedEventArgs e)
        {
            //ExecuteKeyPress("@Left ", 1);
            if (!FlipLeftP2)
            {
                FlipLeftP2 = true;
                ExecuteKeyPress("@Move Left Start ", 1);
            }
            else
            {
                FlipLeftP2 = false;
                ExecuteKeyPress("@Move Left End ", 1);
            }
        }

        //For future?
        //bool FlipDownP1 = false;
        bool FlipDownP2 = false;
        private void DOWNTest_Click(object sender, RoutedEventArgs e)
        {
            if (!FlipDownP2)
            {
                FlipDownP2 = true;
                ExecuteKeyPress("@Crouch Down ", 1);
            }
            else
            {
                FlipDownP2 = false;
                ExecuteKeyPress("@Crouch Up ", 1);
            }
        }

        bool FlipRightP1 = false;
        bool FlipRightP2 = false;
        private void RIGHTTest_Click(object sender, RoutedEventArgs e)
        {
            //ExecuteKeyPress("@Right ", 1);
            if (!FlipRightP2)
            {
                FlipRightP2 = true;
                ExecuteKeyPress("@Move Right Start ", 1);
            }
            else
            {
                FlipRightP2 = false;
                ExecuteKeyPress("@Move Right End ", 1);
            }
        }

        private void UTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Low Punch ", 1);
        }

        private void ITest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Medium Punch ", 1);
        }

        private void OTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@High Punch ", 1);
        }

        private void JTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Low Kick ", 1);
        }

        private void KTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Medium Kick ", 1);
        }

        private void LTest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@High Kick ", 1);
        }

        private void SPACETest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Select ", 1);
        }

        private void DELETETest_Click(object sender, RoutedEventArgs e)
        {
            ExecuteKeyPress("@Back ", 1);
        }
    }
}

namespace Kinect.Extensions
{
    public static class CameraExtensions
    {
        public static bool TrySetAngle(this Camera camera, int angle)
        {
            try
            {
                camera.ElevationAngle = angle;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}